export class Question {
  question: 'string';
  answers: [{
    answer: 'string',
    points: boolean,
    checked: false
  }];
}
