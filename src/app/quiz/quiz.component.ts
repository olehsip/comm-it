import {Component, OnInit} from '@angular/core';
import {Question} from '.././Models/questions.model';
import {QuizService} from '../Services/quiz.service';

@Component({
  selector: 'app-quiz-component',
  templateUrl: 'quiz.component.html',
  styleUrls: ['quiz.component.css']
})
export class QuizComponent implements OnInit {
  currentQuestion = 0;
  totalPoints = 0;
  completed = false;
  questions: Question[] = [];
  pointsForTrueAnswer = 0;

  constructor(private quizService: QuizService) {
  }

  ngOnInit() {
    this.quizService.getQuestions().subscribe(
      (questions: Question[]) => {
        this.questions = questions;
        this.pointsForTrueAnswer = 100 / this.questions.length;
      });
  }

  onCheck(userAnswer, allAnswers) {
    console.log(userAnswer);
    allAnswers.forEach(answer => {

      userAnswer === answer.answer ? answer.checked = true : answer.checked = false;
    });
  }

  onBack() {
    this.completed = false;
    this.currentQuestion--;
  }

  onNext() {
    this.currentQuestion++;
  }

  showTotalPoints() {
    this.totalPoints = 0;
    this.completed = true;
    this.questions.forEach((question) => {
        question.answers.forEach((answer) => {
          answer.points && answer.checked ? this.totalPoints += this.pointsForTrueAnswer : this.totalPoints += 0;
        });
      }
    );
  }
}

